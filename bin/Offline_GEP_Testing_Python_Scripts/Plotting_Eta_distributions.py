"""
This is a plotting script that makes a plot of topocluster multiplicity across 16 eta regions. There is some formatting which makes some dotted lines on the edge of each bin, that the user can turn off down farther down in the code. 

Format for entry:
python3 filename filepath bins Etcut legend_adjustment_x legend_adjustment_y TPave_textsize legendfont TPave_start


A brief description of each argument:
REQUIRED:
file:                   Sets the input .root file to the TFile      
bins:                   Sets the number that we rebin to, initially 2500 bins for these files
cut:                    Sets the specific E_T cut applied to topoclusters, usually in the name of the .root file
legend_adjustment_x:    Sets the additional x position where the legend starts, range from 0 to 1
legend_adjustment_y:    Sets the additional y position where the legend starts, range from 0 to 1
TPave_textsize:         Sets the size of the ATLAS Simulation and related text, three options built in: 0.03, 0.04, 0.05
legend_fontsize:        Sets the size of the legend text, two options built in: 0.03, 0.04
TPave_start:            Sets the starting position of the ATLAS Simulation and related test, range from 0 to 1
################################################################################################################################

Example command line input:

python3 Plotting_Eta_distributions.py /Users/sergeyscoville/Desktop/Projects/ROOT_Github/ROOTwork/Data/18Jul_splitting/hist_Et0p0.root 0 0.0 0.0 0.0 0.04 .7 0.03 0.19


"""


import ROOT
import sys
from helper_functions import *

stylistic = apply_atlas_style()
ROOT.gROOT.SetStyle("AtlasStyle")

if "-h" in sys.argv or "--help" in sys.argv:
    print(__doc__)
    sys.exit(0)

try:
    file                        = ROOT.TFile(sys.argv[1]) 
    bins                        = int(sys.argv[2])              # Sets the number that we rebin to, initially 2500 bins for these files
    cut                         = sys.argv[3]                   # Sets the specific E_T cut 
    legend_adjustment_x         = float(sys.argv[4])            # Sets the additional x position where the legend starts, range from 0 to 1
    legend_adjustment_y         = float(sys.argv[5])            # Sets the additional y position where the legend starts, range from 0 to 1
    TPave_textsize              = sys.argv[6]                   # Sets the size of the ATLAS Simulation and related text, three options built in: 0.03, 0.04, 0.05
    legend_fontsize             = sys.argv[7]                   # Sets the size of the legend text, two options built in: 0.03, 0.04
    TPave_start                 = float(sys.argv[8])            # Sets the starting position of the ATLAS Simulation and related test, range from 0 to 1
except IndexError:
    print("You are missing some of the command line arguments, please look in the code documentation or do python3 N_dist_splitting_vs_nosplitting.py --help")
    sys.exit(0)

# Checking over your command line inputs. The last two are forced, algorithm is set to "NA" and eta_region is obviously the whole thing
bins = input_checker(cut, legend_adjustment_x, legend_adjustment_y, TPave_textsize, legend_fontsize, TPave_start, "NA", "--all")
softie          = False
if "--sk" in sys.argv:
    softie      = True


if "--sk" in sys.argv:
    hist1 = file.Get("h_Calo422SKclusters_eta")
    hist2 = file.Get("h_Calo420SKclusters_eta")
else:
    hist1 = file.Get("h_Calo422TopoClusters_eta") # Change for pre versus post sk: Calo422TopoClusters_N -> Calo422SKclusters_N
    hist2 = file.Get("h_Calo420TopoClusters_eta") # Change for pre versus post sk: Calo420TopoClusters_N -> Calo420SKclusters_N


hist_legend_names = ["Calo 422", "Calo 420"]

hist1.SetName(hist_legend_names[0])
hist2.SetName(hist_legend_names[1])


canvas = ROOT.TCanvas("canvas", "Histograms", 1200, 800)
if "--log" in sys.argv:
    canvas.SetLogy()
canvas.Update()

bin_options = find_divisors(hist1.GetNbinsX())
if bins == 0 and "--suppressbins" not in sys.argv:
    print("The options available to rebin your dataset are", str(bin_options), ". To rebin your dataset, add the integer you wish to end of command line prompt and it will divide the number of bins by this integer to give new binning.")


histogram_total = []
for i in [hist1, hist2]:
    histogram_total.append(i.GetEntries())

hist1, hist2, histogram_numbers = set_y_axis_to_event_fraction(file, [hist1, hist2])

hist_titles = ["Average number of topoclusters as function of #eta", "Average number of topoclusters as function of #eta post SK"]
if softie:
    hist1.SetTitle(hist_titles[1])
else:
    hist1.SetTitle(hist_titles[0])

cut = sys.argv[3]

x_max = get_histograms_xmax([hist1, hist2])
y_max = get_histograms_ymax([hist1, hist2], bins)
real_maximum = get_eta_maximum([hist1, hist2])
histogram_modifiers([hist1, hist2], x_max, real_maximum, 16, cut)
hist1.GetXaxis().SetTitle("#eta")
hist1.GetYaxis().SetTitle("Number of topoclusters/# events")
bin_width = 0.6125
num_ticks = int(hist1.GetXaxis().GetXmax() / bin_width)
hist1.GetXaxis().SetNdivisions(16, False)
hist1.GetXaxis().SetLabelSize(0.023)
hist1.GetXaxis().SetTickLength(0.02)
overflow_bin_set([hist1, hist2])

bin_edges = [-4.9, -4.2875, -3.675, -3.0625, -2.45, -1.8375, -1.225, -0.6125, 0, 0.6125, 1.225, 1.8375, 2.45, 3.0625, 3.675, 4.2875, 4.9 ]


hist1.SetFillColorAlpha(ROOT.kBlue, 0.1)
hist1.SetFillStyle(3144)
hist1.SetLineWidth(2)
hist1.Sumw2()
hist1.Draw("hist")
canvas.Update()

for i in range(len(bin_edges)-1):
    TEST_line = ROOT.TLine()
    TEST_line.SetLineStyle(3)
    TEST_line.DrawLine(bin_edges[i], 0, bin_edges[i], real_maximum*1.1)
    canvas.Update()

starting = float(sys.argv[9])

atlas, sim_internal, hl, min_bia = write_all_but_ETC(TPave_start, TPave_textsize)
atlas.Draw()
sim_internal.Draw()
hl.Draw()
min_bia.Draw()
canvas.Update()

if "NoCut" not in get_save_file_name(file, bins, "N"):
    etcut = write_ET_cut(TPave_start, TPave_textsize, cut)
    etcut.Draw()
    canvas.Update()

for i in range(0, hist2.GetNbinsX()):
    bin_height = hist2.GetBinContent(i+1)
    line = ROOT.TLine()
    line.SetLineWidth(2)  # Set the line thickness
    line.SetLineColor(ROOT.kRed) 
    line.DrawLine(bin_edges[i], bin_height, bin_edges[i+1], bin_height)
    canvas.Update()
hist2.Draw("P same")
hist2.SetEntries(histogram_total[1])
hist2.SetLineWidth(0)
hist2.SetMarkerSize(0)
hist2.SetMarkerColor(ROOT.kRed)
canvas.Update()


canvas.SetName("All_GEP_Algo")
canvas.Update()
legend_sizes = {"0.03": ROOT.TLegend(legend_adjustment_x + 0.2, legend_adjustment_y + 0.3,legend_adjustment_x + 0.3, legend_adjustment_y + 0.42), "0.04": ROOT.TLegend(legend_adjustment_x + 0.2, legend_adjustment_y + 0.3,legend_adjustment_x + 0.33, legend_adjustment_y + 0.42)}

legend = legend_sizes[legend_fontsize]


legend.AddEntry(hist1,hist_legend_names[0].replace("_", " ")) 
legend.AddEntry(line,hist_legend_names[1].replace("_", " "), "l") 
hist1.GetXaxis().SetTitleSize(0.04)
hist1.GetYaxis().SetTitleSize(0.04)
legend.SetTextSize(float(sys.argv[8]))
legend.SetLineWidth(0)  
legend.SetTextSize(float(legend_fontsize))                    # Remove the boundary on the legend
legend.Draw("same")  

canvas.Update()

filepaths = "/Users/sergeyscoville/Desktop/Projects/ROOT_Github/ROOTwork/Doc/"
extensions = ""
if softie:
    extensions += get_save_file_name(file, bins, "eta")+"_SK"
else:
    extensions += get_save_file_name(file, bins, "eta")

if "--sk" in sys.argv:
    canvas.SaveAs(filepaths+"Plots/Plots_w_SK/"+extensions+".png")
    canvas.SaveAs(filepaths+"Plots_PDFs/Plots_PDFs_w_SK/"+extensions+".pdf")
else:
    canvas.SaveAs(filepaths+"Plots/Plots_No_SK/"+extensions+".png")
    canvas.SaveAs(filepaths+"Plots_PDFs/Plots_PDFs_No_SK/"+extensions+".pdf")
